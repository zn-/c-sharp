﻿using System;
using Fei.BaseLib;
using System.Linq;

namespace Treti_ukol
{

    public enum Faculty
    {
        FEI,
        FES,
        FFS,
        NO
    }

    internal class Student
    {
        public string Jmeno { get; set; }
        public int Cislo { get; set; }
        public Faculty Fakulta { get; set; }
    }


    class Program
    {
        static Student[] students = new Student[3];
        
        static void Main(string[] args)
        {
            int choice = 0;
            while (choice != 6)
            {
                Console.WriteLine("Menu:");
                Console.WriteLine("1 - načtení studentů z klávesnice");
                Console.WriteLine("2 - výpis studentů na obrazovku");
                Console.WriteLine("3 - seřazení studentů podle čísla");
                Console.WriteLine("4 - seřazení studentů podle jména");
                Console.WriteLine("5 - seřazení studentů podle fakulty");
                Console.WriteLine("6 - konec programu");

                try
                {
                    choice = Reading.ReadInt("zadejte volbu");
                }
                catch
                {
                    Console.WriteLine("špatně zadaná hodnota");
                    continue;
                }
                if (choice < 1 || choice > 6)
                {
                    Console.WriteLine("špatně zadáno");
                    continue;
                }
                switch (choice)
                {
                    case 1:
                        LoadArray();
                        break;
                    case 2:
                        ReadArray();
                        break;
                    case 3:
                        SortArrayByNumber();
                        break;
                    case 4:
                        SortArrayByName();
                        break;
                    case 5:
                        SortArrayByFaculty();
                        break;
                    case 6:
                        break;
                }
            }
        }


        private static void LoadArray()
        {
            for (int i = 0; i < students.Length; i++)
            {
                string s = "";
                int x = 0;
                string f = "";
                try
                {
                    s = Reading.ReadString("zadejte jméno");
                    x = Reading.ReadInt("zadejte číslo");
                    f = Reading.ReadString("zadejte fakultu");
                    students[i] = new Student();
                    students[i].Jmeno = s;
                    students[i].Cislo = x;
                }
                catch (Exception e)
                {
                    Console.WriteLine("nepodařilo se parsovat číslo");
                    i--;
                }
                if (f.Equals("FEI"))
                {
                    students[i].Fakulta = Faculty.FEI;
                }else if (f.Equals("FES"))
                {
                    students[i].Fakulta = Faculty.FES;
                }else if (f.Equals("FFS"))
                {
                    students[i].Fakulta = Faculty.FFS;
                }
                else
                {
                    students[i].Fakulta = Faculty.NO;
                }
            }
        }

        private static void ReadArray()
        {
            for (int i = 0; i < students.Length; i++)
            {
                Console.WriteLine(students[i].Jmeno + ", " + students[i].Cislo + ", " + students[i].Fakulta);
            }
        }

        private static void SortArrayByNumber()
        {
            Array.Sort(students, delegate (Student students1, Student student2)
            {
                return students1.Cislo.CompareTo(student2.Cislo);
            });
        }

        private static void SortArrayByName()
        {
            Array.Sort(students, delegate (Student students1, Student student2)
            {
                return students1.Jmeno.CompareTo(student2.Jmeno);
            });
        }

        private static void SortArrayByFaculty()
        {
            Array.Sort(students, delegate (Student students1, Student student2)
            {
                return students1.Fakulta.CompareTo(student2.Fakulta);
            });
        }
    }
}
