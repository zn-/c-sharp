﻿using System;
using Fei.BaseLib;
using System.Linq;

namespace Druhy_ukol
{
    class Program
    {
        
        static int[] a = new int[10];
        static void Main(string[] args)
        {
            int choice = 0;
            while (choice != 7)
            {
                Console.WriteLine("Menu:");
                Console.WriteLine("1 - zadání prvků pole z klávesnice");
                Console.WriteLine("2 - výpis pole na obrazovku");
                Console.WriteLine("3 - utřídění pole vzestupně");
                Console.WriteLine("4 - hledání minimálního prvku");
                Console.WriteLine("5 - hledání prvního výskytu zadaného čísla");
                Console.WriteLine("6 - hledání posledního výskytu zadaného čísla");
                Console.WriteLine("7 - konec programu");
                Console.WriteLine("8 - setřídit pole sestupně");
                try
                {
                    choice = Reading.ReadInt("zadejte volbu");
                }
                catch
                {
                    Console.WriteLine("špatně zadaná hodnota");
                    continue;
                }
                if (choice < 1 || choice > 8)
                {
                    Console.WriteLine("špatně zadáno");
                    continue;
                }
                switch (choice)
                {
                    case 1:
                        LoadArray();
                        break;
                    case 2:
                        ReadArray();
                        break;
                    case 3:
                        SortArray();
                        break;
                    case 4:
                        Console.WriteLine("minimum je: " + Minimum());
                        break;
                    case 5:
                        try
                        {
                            int index = FirstAppearance(Reading.ReadInt("zadejte číslo"));
                            if (index == -1)
                            {
                                Console.WriteLine("číslo se v poli nevykystuje");
                            }
                            else
                            {
                                Console.WriteLine("číslo se vyskytuje na indexu: " + index);
                            }
                        }
                        catch
                        {
                            Console.WriteLine("špatně zadáno");
                        }
                        break;
                    case 6:
                        try
                        {
                            int index = LastAppearance(Reading.ReadInt("zadejte číslo"));
                            if(index == -1)
                            {
                                Console.WriteLine("číslo se v poli nevykystuje");
                            }
                            else
                            {
                                Console.WriteLine("číslo se vyskytuje na indexu: " + index);
                            }
                        }
                        catch
                        {
                            Console.WriteLine("špatně zadáno");
                        }
                        break;
                    case 7:
                        break;
                    case 8:
                        SortArrayDesc();
                        break;
                }
            }
        }

        private static void LoadArray()
        {
            for(int i = 0; i < a.Length; i++)
            {
                try
                {
                    a[i] = Reading.ReadInt("zadejte číslo");
                }catch (Exception e)
                {
                    Console.WriteLine("nepodařilo se parsovat číslo");
                    i--;
                }
            }
        }

        private static void ReadArray()
        {
            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine(a[i]);
            }
        }

        private static void SortArray()
        {
            Array.Sort(a);
        }

        private static int Minimum()
        {
           return a.Min();
        }

        private static int FirstAppearance(int n)
        {
            return Array.IndexOf(a, n);
        }

        private static int LastAppearance(int n)
        {
            return Array.LastIndexOf(a, n);
        }

        private static void SortArrayDesc()
        {
            Array.Reverse(a);
        }
    }
}
