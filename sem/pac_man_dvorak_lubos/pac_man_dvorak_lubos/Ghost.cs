﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PacManLubosDvorak
{
    public class Ghost
    {
        private const int PICTURE_SIZE = 40;
        private const int PIXELS_MOVEMENT = 2;
        private const int MIN_CELLS_NEARBY = 0;
        private const int MAX_CELLS_NEARBY = 3;
        public PictureBox Enemy { get; set; }
        private Array values = Enum.GetValues(typeof(Direction));


        private static Image[] images = new Image[4];


        private Random rng = new Random();

        public bool Eatable { get; set; }

        private int type = -1;

        public Direction Direction { get; set; }

        /// <summary>
        /// randomly chose colour of ghost
        /// </summary>
        public Ghost()
        {
            Enemy = new PictureBox();
            type = rng.Next(0, 3);
            switch (type)
            {
                case 0:
                    Enemy.Image = images[0];
                    Eatable = false;
                    break;
                case 1:
                    Enemy.Image = images[1];
                    Eatable = false;
                    break;
                case 2:
                    Enemy.Image = images[2];
                    Eatable = false;
                    break;
            }
            Enemy.Height = PICTURE_SIZE;
            Enemy.Width = PICTURE_SIZE;
            Enemy.Margin = new Padding(0);
            Direction = (Direction)values.GetValue(rng.Next(values.Length));
        }

        static Ghost()
        {
            LoadPictures();
        }

        /// <summary>
        /// changes direction to random direction
        /// </summary>
        public void ChangeDirection()
        {
            Direction = (Direction)values.GetValue(rng.Next(values.Length));
        }

        /// <summary>
        /// changes direction to random one from array of possible directions
        /// </summary>
        /// <param name="arr">array of possible directions</param>
        public void ChangeDirectionByArray(List<Direction> arr)
        {
            Direction = arr[rng.Next(arr.Count)];
        }

        /// <summary>
        /// change direction to opposite direction
        /// </summary>
        /// <returns>opposite direction of ghosts direction</returns>
        public Direction OppositeDirection()
        {
            switch (Direction)
            {
                case Direction.Up:
                    return Direction.Down;
                case Direction.Down:
                    return Direction.Up;
                case Direction.Left:
                    return Direction.Right;
                case Direction.Right:
                    return Direction.Left;
                default:
                    return Direction.Up;
            }
        }

        /// <summary>
        /// checks if ghost intersect with player
        /// </summary>
        /// <param name="player">player</param>
        /// <returns>true if yes, false if not</returns>
        public bool KillPlayer(PictureBox player)
        {
            return player.Bounds.IntersectsWith(Enemy.Bounds);
        }

        /// <summary>
        /// changes ghost to eatable
        /// </summary>
        public void GhostIsEatable()
        {
            Enemy.Image = images[3];
            Eatable = true;
        }

        /// <summary>
        /// changes ghost back to non eatable
        /// </summary>
        public void GhostIsNotEatable()
        {
            switch (type)
            {
                case 0:
                    Enemy.Image = images[0];
                    Eatable = false;
                    break;
                case 1:
                    Enemy.Image = images[1];
                    Eatable = false;
                    break;
                case 2:
                    Enemy.Image = images[2];
                    Eatable = false;
                    break;
            }
        }

        /// <summary>
        /// load pictures form files
        /// </summary>
        private static void LoadPictures()
        {
            FileStream fs = new FileStream("../../imgs/red.jpg", FileMode.Open, FileAccess.Read);
            images[0] = Image.FromStream(fs);
            fs = new FileStream("../../imgs/pink.jpg", FileMode.Open, FileAccess.Read);
            images[1] = Image.FromStream(fs);
            fs = new FileStream("../../imgs/orange.jpg", FileMode.Open, FileAccess.Read);
            images[2] = Image.FromStream(fs);
            fs = new FileStream("../../imgs/blue.png", FileMode.Open, FileAccess.Read);
            images[3] = Image.FromStream(fs);
            fs.Close();
        }

        /// <summary>
        /// move ghost in its direction by 2px
        /// </summary>
        public void GhostMovement()
        {
            Point point = Enemy.Location;
            switch (Direction)
            {
                case Direction.Up:
                    point.Y -= PIXELS_MOVEMENT;
                    break;
                case Direction.Down:
                    point.Y += PIXELS_MOVEMENT;
                    break;
                case Direction.Left:
                    point.X -= PIXELS_MOVEMENT;
                    break;
                case Direction.Right:
                    point.X += PIXELS_MOVEMENT;
                    break;
            }
            Enemy.Location = point;
        }

        /// <summary>
        /// checks if player is at least 3 fields near the ghost
        /// if yes and ghost is not eatable, ghost will start chasing player
        /// if yes and ghost is not eatable, ghost will start running from player
        /// </summary>
        /// <param name="pac">pacman</param>
        /// <returns>returns new ghosts direction od same direction if player is not nearby</returns>
        public Direction IsPlayerNearby(Pacman pac)
        {
            if ((Enemy.Location.X / PICTURE_SIZE - pac.Player.Location.X / PICTURE_SIZE == 0) &&
                (Enemy.Location.Y / PICTURE_SIZE - pac.Player.Location.Y / PICTURE_SIZE <= MAX_CELLS_NEARBY && Enemy.Location.Y / PICTURE_SIZE - pac.Player.Location.Y / PICTURE_SIZE >= MIN_CELLS_NEARBY))
            {
                if (!Eatable)
                    return Direction.Up;
                else
                    return Direction.Down;
            }
            if ((Enemy.Location.X / PICTURE_SIZE - pac.Player.Location.X / PICTURE_SIZE == 0) &&
                (Enemy.Location.Y / PICTURE_SIZE - pac.Player.Location.Y / PICTURE_SIZE >= -MAX_CELLS_NEARBY && Enemy.Location.Y / PICTURE_SIZE - pac.Player.Location.Y / PICTURE_SIZE <= MIN_CELLS_NEARBY))
            {
                if (!Eatable)
                    return Direction.Down;
                else
                    return Direction.Up;
            }
            if ((Enemy.Location.Y / PICTURE_SIZE - pac.Player.Location.Y / PICTURE_SIZE == 0) &&
                (Enemy.Location.X / PICTURE_SIZE - pac.Player.Location.X / PICTURE_SIZE <= MAX_CELLS_NEARBY && Enemy.Location.X / PICTURE_SIZE - pac.Player.Location.X / PICTURE_SIZE >= MIN_CELLS_NEARBY))
            {
                if (!Eatable)
                    return Direction.Left;
                else
                    return Direction.Right;
            }
            if ((Enemy.Location.Y / PICTURE_SIZE - pac.Player.Location.Y / PICTURE_SIZE == 0) &&
                (Enemy.Location.X / PICTURE_SIZE - pac.Player.Location.X / PICTURE_SIZE >= -MAX_CELLS_NEARBY && Enemy.Location.X / PICTURE_SIZE - pac.Player.Location.X / PICTURE_SIZE <= MIN_CELLS_NEARBY))
            {
                if (!Eatable)
                    return Direction.Right;
                else
                    return Direction.Left;
            }
            return Direction;
        }

        /// <summary>
        /// checks if ghost is at crossroad, if yes change its location except the direction from which ghost came
        /// </summary>
        /// <param name="mapGrid">map</param>
        /// <returns>returns array of ghosts posiible directions or null if ghost is not at crossroad</returns>
        public List<Direction> CheckCrossroad(TableLayoutPanel mapGrid)
        {
            List<Direction> arr;
            Control c1, c3, c5, c7;
            if (Enemy.Location.X % PICTURE_SIZE == 0 && Enemy.Location.Y % PICTURE_SIZE == 0)
            {
                c1 = mapGrid.GetChildAtPoint(new Point(Enemy.Location.X, Enemy.Location.Y - 1));
                c3 = mapGrid.GetChildAtPoint(new Point(Enemy.Location.X, Enemy.Location.Y + PICTURE_SIZE));
                c5 = mapGrid.GetChildAtPoint(new Point(Enemy.Location.X - 1, Enemy.Location.Y));
                c7 = mapGrid.GetChildAtPoint(new Point(Enemy.Location.X + PICTURE_SIZE, Enemy.Location.Y));
                if ((c1 == null || c1.GetType() == typeof(CoinPictureBox)) && (c3 == null || c3.GetType() == typeof(CoinPictureBox)) &&
                    (c5 == null || c5.GetType() == typeof(CoinPictureBox)) && (c7 != null && c7.GetType() == typeof(Panel)))
                {
                    arr = new List<Direction> { Direction.Up, Direction.Down, Direction.Left };
                    arr.Remove(OppositeDirection());
                }
                else if ((c3 == null || c3.GetType() == typeof(CoinPictureBox)) && (c5 == null || c5.GetType() == typeof(CoinPictureBox)) &&
                    (c7 == null || c7.GetType() == typeof(CoinPictureBox)) && (c1 != null && c1.GetType() == typeof(Panel)))
                {
                    arr = new List<Direction> { Direction.Right, Direction.Down, Direction.Left };
                    arr.Remove(OppositeDirection());
                }
                else if ((c1 == null || c1.GetType() == typeof(CoinPictureBox)) && (c5 == null || c5.GetType() == typeof(CoinPictureBox)) &&
                    (c7 == null || c7.GetType() == typeof(CoinPictureBox)) && (c3 != null && c3.GetType() == typeof(Panel)))
                {
                    arr = new List<Direction> { Direction.Up, Direction.Right, Direction.Left };
                    arr.Remove(OppositeDirection());
                }
                else if ((c1 == null || c1.GetType() == typeof(CoinPictureBox)) && (c3 == null || c3.GetType() == typeof(CoinPictureBox)) &&
                    (c7 == null || c7.GetType() == typeof(CoinPictureBox)) && (c5 != null && c5.GetType() == typeof(Panel)))
                {
                    arr = new List<Direction> { Direction.Up, Direction.Down, Direction.Right };
                    arr.Remove(OppositeDirection());
                }
                else if ((c1 == null || c1.GetType() == typeof(CoinPictureBox)) && (c3 == null || c3.GetType() == typeof(CoinPictureBox)) &&
                    (c5 == null || c5.GetType() == typeof(CoinPictureBox)) && (c7 == null || c7.GetType() == typeof(CoinPictureBox)))
                {
                    arr = new List<Direction> { Direction.Up, Direction.Down, Direction.Left, Direction.Right };
                    arr.Remove(OppositeDirection());
                }
                else
                {
                    return null;
                }
                return arr;
            }
            return null;
        }
    }
}
