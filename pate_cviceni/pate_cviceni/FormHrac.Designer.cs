﻿namespace pate_cviceni
{
    partial class FormHrac
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ok = new System.Windows.Forms.Button();
            this.storno = new System.Windows.Forms.Button();
            this.jmenoTF = new System.Windows.Forms.TextBox();
            this.comboBox = new System.Windows.Forms.ComboBox();
            this.golTF = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.golTF)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Jméno:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Klub:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Góly:";
            // 
            // ok
            // 
            this.ok.Location = new System.Drawing.Point(51, 119);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(75, 23);
            this.ok.TabIndex = 3;
            this.ok.Tag = "ok";
            this.ok.Text = "OK";
            this.ok.UseVisualStyleBackColor = true;
            this.ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // storno
            // 
            this.storno.Location = new System.Drawing.Point(158, 119);
            this.storno.Name = "storno";
            this.storno.Size = new System.Drawing.Size(75, 23);
            this.storno.TabIndex = 4;
            this.storno.Tag = "storno";
            this.storno.Text = "Storno";
            this.storno.UseVisualStyleBackColor = true;
            this.storno.Click += new System.EventHandler(this.storno_Click);
            // 
            // jmenoTF
            // 
            this.jmenoTF.Location = new System.Drawing.Point(81, 9);
            this.jmenoTF.Name = "jmenoTF";
            this.jmenoTF.Size = new System.Drawing.Size(100, 20);
            this.jmenoTF.TabIndex = 5;
            this.jmenoTF.Tag = "jmenoTF";
            // 
            // comboBox
            // 
            this.comboBox.FormattingEnabled = true;
            this.comboBox.Location = new System.Drawing.Point(81, 43);
            this.comboBox.Name = "comboBox";
            this.comboBox.Size = new System.Drawing.Size(121, 21);
            this.comboBox.TabIndex = 7;
            this.comboBox.Tag = "comboBox";
            // 
            // golTF
            // 
            this.golTF.Location = new System.Drawing.Point(81, 77);
            this.golTF.Name = "golTF";
            this.golTF.Size = new System.Drawing.Size(120, 20);
            this.golTF.TabIndex = 8;
            this.golTF.Tag = "golTF";
            // 
            // FormHrac
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(271, 183);
            this.Controls.Add(this.golTF);
            this.Controls.Add(this.comboBox);
            this.Controls.Add(this.jmenoTF);
            this.Controls.Add(this.storno);
            this.Controls.Add(this.ok);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormHrac";
            this.Text = "FormHrac";
            ((System.ComponentModel.ISupportInitialize)(this.golTF)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button ok;
        private System.Windows.Forms.Button storno;
        private System.Windows.Forms.TextBox jmenoTF;
        private System.Windows.Forms.ComboBox comboBox;
        private System.Windows.Forms.NumericUpDown golTF;
    }
}