﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pate_cviceni
{
    public partial class Export : Form
    {
        public Hraci Hraci { get; set; }
        public Export(Hraci hraci)
        {
            InitializeComponent();
            Hraci = hraci;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int pocetHracu = 0;
            Seznam seznam = new Seznam();
            foreach(Hrac h in Hraci.seznam)
            {
                if (cbNone.Checked)
                {
                    if(h.Klub == FotbalovyKlub.None)
                    {
                        seznam.Add(h);
                        pocetHracu++;
                    }
                }
                if (cbPorto.Checked)
                {
                    if (h.Klub == FotbalovyKlub.Porto)
                    {
                        seznam.Add(h);
                        pocetHracu++;
                    }
                }
                if (cbArsenal.Checked)
                {
                    if (h.Klub == FotbalovyKlub.Arsenal)
                    {
                        seznam.Add(h);
                        pocetHracu++;
                    }
                }
                if (cbReal.Checked)
                {
                    if (h.Klub == FotbalovyKlub.Real)
                    {
                        seznam.Add(h);
                        pocetHracu++;
                    }
                }
                if (cbChelsea.Checked)
                {
                    if (h.Klub == FotbalovyKlub.Chelsea)
                    {
                        seznam.Add(h);
                        pocetHracu++;
                    }
                }
                if (cbBarcelona.Checked)
                {
                    if (h.Klub == FotbalovyKlub.Barcelona)
                    {
                        seznam.Add(h);
                        pocetHracu++;
                    }
                }
            }
            Hraci.seznam.Reset();

            if (pocetHracu != 0)
            {
                SaveFileDialog save = new SaveFileDialog();

                save.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                save.FilterIndex = 2;
                save.RestoreDirectory = true;

                if (save.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter writer = new StreamWriter(save.OpenFile());

                    foreach (Hrac h in seznam)
                    {

                        writer.WriteLine(h.Jmeno);
                        writer.WriteLine(FotbalovyKlubInfo.DejNazev(h.Klub));
                        writer.WriteLine(h.GolPocet.ToString());
                    }
                    seznam.Reset();
                    writer.Dispose();
                    writer.Close();
                    this.DialogResult = DialogResult.OK;
                    
                }
            }
            this.Close();

        }
    }
}
