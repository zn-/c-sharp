﻿namespace pate_cviceni
{
    partial class FormKluby
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.goly = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textAreaKlub = new System.Windows.Forms.ListView();
            this.ok = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "góly:";
            // 
            // goly
            // 
            this.goly.Location = new System.Drawing.Point(16, 39);
            this.goly.Name = "goly";
            this.goly.ReadOnly = true;
            this.goly.Size = new System.Drawing.Size(100, 20);
            this.goly.TabIndex = 1;
            this.goly.Tag = "goly";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "kluby";
            // 
            // textAreaKlub
            // 
            this.textAreaKlub.HideSelection = false;
            this.textAreaKlub.Location = new System.Drawing.Point(19, 98);
            this.textAreaKlub.Name = "textAreaKlub";
            this.textAreaKlub.Size = new System.Drawing.Size(235, 185);
            this.textAreaKlub.TabIndex = 3;
            this.textAreaKlub.Tag = "textAreaKlub";
            this.textAreaKlub.UseCompatibleStateImageBehavior = false;
            // 
            // ok
            // 
            this.ok.Location = new System.Drawing.Point(178, 290);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(75, 23);
            this.ok.TabIndex = 4;
            this.ok.Tag = "ok";
            this.ok.Text = "OK";
            this.ok.UseVisualStyleBackColor = true;
            this.ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // FormKluby
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(270, 334);
            this.Controls.Add(this.ok);
            this.Controls.Add(this.textAreaKlub);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.goly);
            this.Controls.Add(this.label1);
            this.Name = "FormKluby";
            this.Text = "FormKluby";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox goly;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView textAreaKlub;
        private System.Windows.Forms.Button ok;
    }
}