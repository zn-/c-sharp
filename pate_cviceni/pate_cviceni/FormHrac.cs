﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pate_cviceni
{
    public partial class FormHrac : Form
    {
        public Hrac Hrac { get; set; }
        public FormHrac(Hrac hrac)
        {
            InitializeComponent();
            comboBox.DataSource = Enum.GetValues(typeof(FotbalovyKlub));
            if(hrac != null)
            {
                jmenoTF.Text = hrac.Jmeno;
                comboBox.SelectedItem = hrac.Klub;
                golTF.Text = hrac.GolPocet.ToString();
            }
        }

        private void ok_Click(object sender, EventArgs e)
        {
            FotbalovyKlub f;
            if(jmenoTF.Text != null && Enum.TryParse<FotbalovyKlub>(comboBox.SelectedValue.ToString(), out f) != false && golTF.Text != "")
            {
                Hrac = new Hrac(jmenoTF.Text, f, Int32.Parse(golTF.Text));
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Špatně zadané údaje", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void storno_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
