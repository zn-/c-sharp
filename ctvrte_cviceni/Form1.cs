﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cv_04
{
    public partial class Form1 : Form
    {
        Random random;
        Stats stats;
        string[] fruits = new string[] { "APPLE", "MANGO", "PAPAYA", "BANANA", "GUAVA", "PEAR" };
        Boolean started = false;
        Boolean paused = false;
        Boolean gameOver = false;
        string word;
        int wordLength;
        int wordIndex = 0;


        public Form1()
        {
            InitializeComponent();
            random = new Random();
            stats = new Stats();
            stats.UpdatedStats += UPD_UpdatedStats;
            timer1.Tick += new EventHandler(Timer1_Tick);
            allLettersToolStripMenuItem.Checked = true;
        }

        private void UPD_UpdatedStats(object sender, EventArgs e)
        {
            correctLabel.Text = "Correct: " + stats.Correct.ToString();
            missedLabel.Text = "Missed: " + stats.Missed.ToString();
            accurancyLabel.Text = "Accuracy: " + stats.Accuracy.ToString();
        }


        private void Timer1_Tick(object sender, EventArgs e)
        {
            if(firstLetterToolStripMenuItem.Checked == true || allLettersToolStripMenuItem.Checked == true)
            {
                gameListBox.Items.Add((Keys)random.Next('A', 'Z'));
                if (gameListBox.Items.Count == 6)
                {
                    gameOver = true;
                    paused = false;
                    started = false;
                    timer1.Stop();
                    MessageBox.Show("Game over!");
                }
            }
            else
            {
                gameListBox.Items.Add(fruits[random.Next(0, fruits.Length)]);
                if (gameListBox.Items.Count == 6)
                {
                    gameOver = true;
                    paused = false;
                    started = false;
                    timer1.Stop();
                    MessageBox.Show("Game over!");
                }
                
            }
            
        }

        private void LB_KeyDown(object sender, KeyEventArgs e)
        {
            if(allLettersToolStripMenuItem.Checked == true)
            {
                if (gameListBox.Items.Contains(e.KeyCode))
                {
                    gameListBox.Items.Remove(e.KeyCode);
                    gameListBox.Refresh();
                    stats.Update(true);
                }
                else
                {
                    stats.Update(false);
                }
            }
            else if(firstLetterToolStripMenuItem.Checked == true)
            {
                if (gameListBox.Items.IndexOf(e.KeyCode) == 0)
                {
                    gameListBox.Items.Remove(e.KeyCode);
                    gameListBox.Refresh();
                    stats.Update(true);
                }
                else
                {
                    stats.Update(false);
                }
            }
            else
            {
                if(wordIndex == 0)
                {
                    word = gameListBox.Items[0].ToString();
                    wordLength = word.Length;
                }

                if (word[wordIndex] == (char)e.KeyCode)
                {
                    wordIndex++;
                }
                else
                {
                    stats.Update(false);
                }
                
                if(wordIndex == wordLength)
                {
                    wordLength = 0;
                    wordIndex = 0;
                    gameListBox.Items.RemoveAt(0);
                    gameListBox.Refresh();
                    stats.Update(true);
                }
               
            }
            

            if (timer1.Interval > 400)
            {
                timer1.Interval -= 60;
            }
            else if (timer1.Interval > 250)
            {
                timer1.Interval -= 15;
            }
            else if (timer1.Interval > 150)
            {
                timer1.Interval -= 8;
            }
            int value = 800 - timer1.Interval;
            if (value < 0 || value > 800) {
                paused = false;
                started = false;
                timer1.Stop();
                MessageBox.Show("interval je mimo");
            }
            else
            {
                difficultyProgressBar.Value = value;
            }
        }

        private void StartOnClick(object sender, EventArgs e)
        {
            if(started == false)
            {
                if(gameOver == true)
                {
                    correctLabel.Text = "Correct: 0";
                    missedLabel.Text = "Missed: 0";
                    accurancyLabel.Text = "Accuracy: 0";
                    stats.Accuracy = 0;
                    stats.Correct = 0;
                    stats.Missed = 0;
                    gameListBox.Items.Clear();
                    timer1.Interval = 800;
                    difficultyProgressBar.Value = 0;
                }
                gameOver = false;
                paused = false;
                started = true;
                if (wordsToolStripMenuItem.Checked == true)
                {
                    gameListBox.ColumnWidth = 400;
                    this.Width = 2000;
                }
                else
                {
                    this.Width = 809;
                    gameListBox.ColumnWidth = 0;
                }
                timer1.Start();
            }
            
        }

        private void EndOnClick(object sender, EventArgs e)
        {
            gameOver = false;
            paused = true;
            started = false;
            timer1.Stop();
        }

        private void RestartOnClick(object sender, EventArgs e)
        {
            gameOver = false;
            paused = false;
            started = true;
            if (wordsToolStripMenuItem.Checked == true)
            {
                gameListBox.ColumnWidth = 450;
                this.Width = 2000;
            }
            else
            {
                this.Width = 809;
                gameListBox.ColumnWidth = 0;
            }
            stats.Accuracy = 0;
            stats.Correct = 0;
            stats.Missed = 0;
            correctLabel.Text = "Correct: 0";
            missedLabel.Text = "Missed: 0";
            accurancyLabel.Text = "Accuracy: 0";
            gameListBox.Items.Clear();
            timer1.Interval = 800;
            difficultyProgressBar.Value = 0;
            timer1.Start();
        }

        private void regimeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(started == true || paused == true)
            {
                firstLetterToolStripMenuItem.Enabled = false;
                allLettersToolStripMenuItem.Enabled = false;
                wordsToolStripMenuItem.Enabled = false;
            }
            else
            {
                firstLetterToolStripMenuItem.Enabled = true;
                allLettersToolStripMenuItem.Enabled = true;
                wordsToolStripMenuItem.Enabled = true;
            }
        }

        private void ItemOnClickFL(object sender, EventArgs e)
        {
            firstLetterToolStripMenuItem.Checked = true;
            allLettersToolStripMenuItem.Checked = false;
            wordsToolStripMenuItem.Checked = false;
        }

        private void ItemOnClickAL(object sender, EventArgs e)
        {
            firstLetterToolStripMenuItem.Checked = false;
            allLettersToolStripMenuItem.Checked = true;
            wordsToolStripMenuItem.Checked = false;
        }

        private void ItemOnClickW(object sender, EventArgs e)
        {
            firstLetterToolStripMenuItem.Checked = false;
            allLettersToolStripMenuItem.Checked = false;
            wordsToolStripMenuItem.Checked = true;
        }
    }
}
